package app

import (
	grpcapp "grpc-service-ref/internal/app/grpc"
	"log/slog"
	"time"
)

type App struct {
	GRPCSrv *grpcapp.App
}

func New(log *slog.Logger, grpcPort int, storagePath string, tokenTTL time.Duration) *App {
	// TODO: инициализировать хранилище

	// TODO: инициализировать auth service

	grpcApp := grpcapp.NewApp(log, grpcPort)

	return &App{GRPCSrv: grpcApp}
}
