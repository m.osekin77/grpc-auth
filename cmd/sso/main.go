package main

import (
	"grpc-service-ref/internal/app"
	"grpc-service-ref/internal/config"
	"grpc-service-ref/internal/logger"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	cfg := config.MustLoad()

	log := logger.SetupLogger(cfg.Env)

	log.Info("data", slog.Any("config", cfg))

	application := app.New(log, cfg.GRPC.Port, cfg.StoragePath, cfg.TokenTTL)

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGTERM, syscall.SIGINT)

	go application.GRPCSrv.MustRun()
	<-stop
	application.GRPCSrv.Stop()

	log.Info("application stopped")
	// TODO: запустить gRPC-сервер приложения
}
